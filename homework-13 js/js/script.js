"use strict";
const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const ul = document.createElement('ul');
function showList(array, domElement){
    if (typeof domElement === undefined ) {
             domElement = document.body;
         }
    const arrList = (array.map(function (name){
        return `<li>${name}</li>`;
    }))
     for (const arrayItem of arrList){
            let arrElement = document.createElement('li');
            arrElement.innerHTML = arrayItem;
            ul.append(arrElement);
            domElement.append(ul);
        }
     domElement.append(ul);
}
showList(array, document.body);
