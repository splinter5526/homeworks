"use strict";
let inputFocus = document.querySelector(".price");
inputFocus.addEventListener('focus', function (){
    if(focus) {
        inputFocus.style.border = '5px solid green';
        inputFocus.style.borderRadius = '5px';
    }
})
inputFocus.addEventListener('blur', function () {
    inputFocus.style.border = null;
    inputFocus.style.borderRadius = null;
    let inputValue = inputFocus.value;
    if (inputValue > 0 && document.querySelector('span') == null) {
        let span = document.createElement('span');
        span.className = "span";
        span.innerText = `Текущая цена: $ ${inputValue}`;
        document.body.prepend(span);
        inputFocus.style.backgroundColor = 'green';
        let buttonClose = document.createElement('button');
        buttonClose.innerText = 'x';
        buttonClose.style.marginLeft = '5px';
        buttonClose.style.borderRadius = '13px';
        buttonClose.style.margin = `3px`
        span.append(buttonClose);
        if (buttonClose.addEventListener('click', function (evt){
            inputFocus.value = null;
            inputFocus.style.backgroundColor = null;
            buttonClose.style.display = 'none';
            span.style.display = 'none';
        })
        );
    }
    else if (inputValue <= 0 && document.querySelector('.error') == null) {
        inputFocus.style.backgroundColor = 'red';
        let errorSpan = document.createElement('div');
        errorSpan.className = 'error';
        errorSpan.innerText = 'Please enter correct price';
        errorSpan.style.color = 'red';
        document.body.append(errorSpan);
    }
});
